

export const skill = [
        { skill: "HTML"},
        { skill: "CSS"}, 
        { skill: "JavaScript"},
        { skill: "Reactjs"},
        { skill: "Nodejs"},
        { skill: "Redux"},
        { skill: "Express-js" },
        { skill: "MongoDB" },
        { skill: "Chakra ui"},
];


export const tool = [
    { skill: "Linux"}, 
    { skill: "Git"},
    { skill: "VS Code"},
    { skill: "Post Man" },
];

export const tech = [
    { skill: "JavaScript"}, 
    { skill: "ReactJs"},
    { skill: "Nodejs"},
    { skill: "Redux"},
    { skill: "Express-js"},
    { skill: "MongoDB"},
    { skill: "Chakra ui"}
]