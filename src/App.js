import { Flex } from "@chakra-ui/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header";
import HomeScreen from "./screens/HomeScreen";
import ProjectsScreen from "./screens/ProjectsScreen";





function App() {
  return (
    <BrowserRouter>
			<Header />

			<Flex 
      as='main'
      mt='147px'
      direction='column'
      py='6'
      px='6'
      bgColor='black' >
       <Routes>
          <Route path="/" element={<HomeScreen/>}/>
          <Route path="/project" element={<ProjectsScreen/>}/>
       </Routes>
      </Flex>
		</BrowserRouter>
  );
}

export default App;
