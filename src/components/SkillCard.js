import { Flex, Icon, Heading } from "@chakra-ui/react";

export const SkillCard = ({children}) => {
    return (
        <>
            <Flex 
                direction='flex'
                justifyContent='flex'
                border='2px solid white'
                py='2'
                px='2'
                borderRadius='10'
            >
                <Heading as="h6" fontSize="sm">
                {children}
                </Heading>
            </Flex>
        
        </>
    )
};



export const ToolsUsedCard = ({children}) => {
    return (
        <>
            <Flex 
                direction='flex'
                justifyContent='flex'
                bgColor='teal'
                py='2'
                px='2'
                borderRadius='10'
            >
                <Heading as="h5" fontSize="sm">
                {children}
                </Heading>
            </Flex>
        
        </>
    )
};


