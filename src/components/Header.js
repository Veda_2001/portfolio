import { Flex, Heading, Icon, } from '@chakra-ui/react';
import { AiOutlineProject, AiOutlineHome } from 'react-icons/ai';
import { LuClipboardList} from 'react-icons/lu';
import HeaderMenuItem from './HeaderMenuItem';

const Header = () => {

	return (
		<Flex>
				<Flex
				as='header'
				wrap='wrap'
				justifyContent='space-between'
				py='3'
				px='6'
				bgColor='black'
				w='100%'
				pos='fixed'
				top='0'
				left='0'
				zIndex='9999'>
					<Heading
						as='h1'
						color='whiteAlpha.800'
						fontWeight='bold'
						size='md'
						letterSpacing='wide'>
						Portfolio
					</Heading>
					<Flex 
					w='200px'
					p="3"
                    direction="row"
					alignItems='center'
                    justifyContent="center"
                    wrap="wrap"
					mt='5'
					gap='5'
					borderRadius='10'
					border='1px solid white'
					>
						<HeaderMenuItem 
						 	url='/'
							icon={<Icon as={AiOutlineHome}  w='5' h='5' />}
							label='HOME'
						/>
						<HeaderMenuItem 
						 	url='/project'
							 icon={<Icon as={AiOutlineProject}  w='5' h='5' />}
							label='PROJECT'
						/>
						
					</Flex>


			</Flex>
			
		</Flex>
		

	);
};

export default Header;
