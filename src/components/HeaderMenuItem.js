import { Flex, Link,  Heading } from '@chakra-ui/react';
import { Link as RouterLink} from "react-router-dom";


const HeaderMenuItem = ({ url,label, icon}) => {
	return (

		<Flex
            direction='flex'
            justifyContent='center'
            letterSpacing='wide'
			textTransform='uppercase'
            p='2'
            alignItems='center'
			color='whiteAlpha.700'
			border= '2px solid white'
            borderRadius='15'
            _hover={{bgColor: 'gray.700'}}
        >
			<Link
				as={RouterLink}
				to={url}
				_hover={{textDecor: 'none'}}
			>
				<Flex 
                    p='2'
                    justifyContent='center'
                    alignItems='center'
                >
                    {icon}
                </Flex>
                    
                <Heading
                    as='h3' 
                    fontSize='sm' 
                    mt='2'
                    >
                    {label}
                </Heading>
			</Link>

		</Flex>

		// // <Link
		// // 	as={RouterLink}
		// // 	to={url}
		// // 	fontSize='sm'
		// // 	letterSpacing='wide'
		// // 	textTransform='uppercase'
		// // 	mr='5'
		// // 	display='flex'
		// // 	alignItems='center'
		// // 	color='whiteAlpha.800'
		// // 	mb={{ base: '2', md: 0 }}
		// // 	_hover={{ textDecor: 'none', color: 'whiteAlpha.600' }}>
		// // 		<Box
		// // 		// direction='flex'
        // //         justifyContent='center'
        // //         border='2px solid white'
        // //         py='2'
        // //         px='2'
		// // 		_hover={{bgColor:'gray'}}
        // //         borderRadius='10'>
		// // 		{icon}
		// // 		{label}
		// // 		</Box>
			
		// </Link>
	);
};

export default HeaderMenuItem;
