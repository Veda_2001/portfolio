import { 
    Text,  Button,   Icon,
    Image,  Heading,   Link , 
    Divider,  Box,  Card, CardBody, Stack, CardFooter, 
    Popover, PopoverTrigger, PopoverContent, PopoverHeader, PopoverCloseButton, PopoverBody, } from "@chakra-ui/react";
import {HiArrowUpRight,HiOutlineChevronRight} from 'react-icons/hi2';
import {AiFillGitlab} from 'react-icons/ai';

export const ProjectCard = ({src, description, name,details, url}) => {
    

    return (
    
        <Card maxW='lg' bgColor='gray.700' color='whiteAlpha.800'>
            <CardBody>
                <Image
                src={src}
                alt={name}
                borderRadius='lg'
                w='full'
                h='300'
                />
            <Stack mt='6' spacing='3'>
            <Heading size='md'>{name}</Heading>
            <Text>{description} </Text>
            </Stack>
            </CardBody>
            <Divider />
            <CardFooter>
                <Box>  
                    <Link href={url}>
                        <Button bgColor='teal' mr='5' _hover={{bgColor:'cyan'}}>
                            <Icon as={AiFillGitlab} />
                        </Button>
                    </Link>
                                        
                <Popover placement="top-end">
                    <PopoverTrigger>
                    <Button
                        bgColor='teal' 
                        mr='5' 
                        letterSpacing='wide'
                        _hover={{bgColor:'cyan'}}
                        >Details
                            <Icon as={HiOutlineChevronRight} ml='2'/>
                        </Button>
                    </PopoverTrigger>
                    <PopoverContent bg="teal" color="black">
                    <PopoverHeader fontWeight="bold" fontSize="xxl">
                        Details
                    </PopoverHeader>
                    <PopoverCloseButton/>
                    <PopoverBody>{details}</PopoverBody>
                    </PopoverContent>
                </Popover>
                </Box>
        </CardFooter>
    </Card>
            
    );
}

