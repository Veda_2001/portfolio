import { Box, Flex, Text, Grid, Image, Divider, Heading, Button,Link, Icon} from "@chakra-ui/react";
import {SkillCard } from "../components/SkillCard";
import { skill, tool } from "../skill";
import {AiFillGitlab, AiFillLinkedin} from 'react-icons/ai';

const HomeScreen = () => {
    return(
        <>
            <Flex
            wrap='wrap'
            direction={{ base: 'column', sm: 'row' }} 
            >
                <Grid templateColumns='2fr 6fr' gap='10' direction={{ base: 'column', sm: 'row' }}>
                    {/* column 1 */}
                    <Flex
                            direction='column'
                            p='5'
                            color='whiteAlpha.800'
                            justifyContent='center'
                            alignItems='center'
                            bgColor='gray.700'
                            borderRadius='10'
                        >
                            <Image h='200px' src='/images/vedanti_patkar.jpg' alt='cat' borderRadius='10' />
                            
                            <Heading
                                as='h2'
                                fontSize='2xl'
                                mt='5'
                            >
                                Vedanti Umesh Patkar
                            </Heading>
                            <Text 
                                mt='2'
                                fontSize='lg'
                            >
                                FullStack Developer
                            </Text>

                            <Divider mt='5' /> 

                            <Flex
                            direction='column'
                            mt='5'
                            p='5'
                            color='whiteAlpha.800'
                            justifyContent='center'
                            alignItems='center'
                            bgColor='gray.500'
                            borderRadius='10'
                            >
                                <Heading
                                    as='h4'
                                    fontSize='md'
                                >
                                    Contact
                                </Heading>
                                <Text>
                                    Email:
                                    <Link href='mailto:patkarvedanti@gmail.com'>
                                     patkarvedanti@gmail.com
                                    </Link>
                                </Text>
                                
                                <Flex
                                    mt='2'
                                    border='1px solid white'
                                    borderRadius='10'
                                    p='3'
                                    gap='5'
                                >
                                    <Link href="https://gitlab.com/Veda_2001">
                                        <Icon as={AiFillGitlab} h='7' w='7'/>
                                    </Link>
                                    <Link href="www.linkedin.com/in/vedanti-patkar-43b7a2255/">
                                        <Icon as={AiFillLinkedin} h='7' w='7'/> 
                                    </Link>
                                </Flex>
                                
                            </Flex>

                    </Flex>

                    {/* column 2 */}

                    <Flex
                    direction='column'
                    p='5'
                    color='whiteAlpha.800'
                    bgColor='gray.700'
                    borderRadius='10'
                    >
                        <Box>
                        <Heading
                            as='h4'
                            fontSize='3xl'
                        >
                            ABOUT ME
                        </Heading>
                        <Divider mt='3'/>
                        <Text
                            as='p'
                            px='5'
                            fontSize='md'   
                            mt='2'                     
                        >
                        Hello, I am Vedanti Umesh Patkar , I aim to kickstart my career in full stack development by applying my foundational knowledge in front-end and back-end technologies.
                        Eager to contribute to projects and learn, I seek a role where I can enhance my skills,
                        collaborate with experienced professionals and again experience. 
                        </Text>
                        <Text
                            as='p'
                            px='5'
                            fontSize='md' 
                        >
                            I have completed my Bachelors in Enginnering with specialization in Information Technology from Padmabhushan Vasantdada Patil Pratishthan's College of Engineering and Visual Arts,
                            where I acquired skills in programming and web development. 
                        </Text>
                        <Text 
                        as='p'
                        px='5'
                        fontSize='md' 
                        >
                            During my studies, I was captivated by the power and versatility of React, which led me to specialize in this amazing technology stack.
                        </Text>
                        </Box>
                        <Flex
                            direction='row'
                            gap='10'
                            p='5'
                            alignItems='center'
                            justifyContent='center'
                        >
                        <Flex
                            direction='column'
                            bgColor='gray.900'
                            mt='3'
                            borderRadius='10'
                            w='400px'
                            h='250px'
                        >
                            <Heading 
                                as='h5'
                                fontSize='lg'
                                p='5'
                            >
                                My Skills
                            </Heading>
                            <Flex
                            p="2"
                            mt='3'
                            direction="row"
                            justifyContent="center"
                            wrap="wrap"
                            gap="5"
                            >
                            {skill.map((prop) => (
                                <SkillCard>{prop.skill}</SkillCard>
                            ))}
                        </Flex>

                        </Flex>
                        <Flex
                            direction='column'
                            bgColor='gray.900'
                            mt='3'
                            borderRadius='10'
                            w='400px'
                            h='250px'
                        >
                            <Heading 
                                as='h5'
                                fontSize='lg'
                                p='5'
                            >
                                Tools
                            </Heading>
                            <Flex
                            p="2"
                            mt='3'
                            direction="row"
                            justifyContent="center"
                            wrap="wrap"
                            gap="5"
                            >
                           {tool.map((prop) => (
                                <SkillCard icon={prop.icon}>{prop.skill}</SkillCard>
                            ))}
                    </Flex>

                        </Flex>
                        </Flex>
                        
                        
                    </Flex>
                    


                </Grid>

            </Flex>
            
            
        </>
        
    )
}

export default HomeScreen;