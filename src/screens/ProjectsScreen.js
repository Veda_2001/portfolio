import { Flex, Heading,} from "@chakra-ui/react";
import { ProjectCard} from "../components/ProjectCard";


const ProjectsScreen = () => {
    

    return(

        <>
        <Heading as='h2' fontSize='4xl' mb='5' px='3' color='whiteAlpha.800'>My Projects</Heading>
        <Flex direction={{ base: 'column', sm: 'row' }} justifyContent="center" wrap="wrap" gap='10'>

        <ProjectCard
            details='Payment Gateway, Admin access and can view and update, can make delivery, users can place order. Login/ Register Page and Authorization using JWT.'
            src='/images/foodies.png'
            name='Foodies'
            url="https://gitlab.com/Veda_2001/foodies"
            description= 'An Restaurant Food Ordering Website completey built in full stack using ReactJs, NodeJs, ExpressJs, Redux, Chakra UI and MongoDb as Database. A restaurants online Website where Users can place food order and make payment online. '
        />

        <ProjectCard
            details='Login/ Register Page, Payment Gateway, Authorization using JWT, Admin access and can view and update, can make delivery, users can place order and make payment.'
            src='/images/rststore.png'
            name='Rst Store'
            url="https://gitlab.com/Veda_2001/rststore"
            description= 'An E-commerce Clothing Website completey built in full stack using ReactJs, NodeJs, ExpressJs, Redux, Chakra UI and MongoDb as Database.'
            
        />
        
        </Flex>
        </>
    )
};

export default ProjectsScreen;